﻿using System;
using BombRush.Core;
using UnityEngine;

namespace BombRush.Bombs
{
    public class BombBehaviour : MonoBehaviour
    {
        private static Collider[] CollidersBuffer = new Collider[64];

        public event Action DetonatedEvent; 
        
        public float ExplosionRadius => _explosionRadius;

        private float _explosionDamage;
        private float _explosionRadius;
        private LayerMask _physicsMask;


        public void Initialize(float explosionDamage, float explosionRadius, LayerMask physicsMask)
        {
            _physicsMask = physicsMask;
            _explosionRadius = explosionRadius;
            _explosionDamage = explosionDamage;
        }

        private void OnCollisionEnter()
        {
            Vector3 bombPosition = transform.position;

            int overlapCount =
                Physics.OverlapSphereNonAlloc(bombPosition, ExplosionRadius, CollidersBuffer, _physicsMask);

            for (int i = 0; i < overlapCount; i++)
            {
                Collider overlapCollider = CollidersBuffer[i];
                var damageable = overlapCollider.GetComponent<IDamageable>();

                if (damageable == null)
                {
                    continue;
                }

                bool inSight = false;
                Vector3 hitPoint = Vector3.zero;

                if (Physics.Linecast
                (
                    bombPosition,
                    overlapCollider.transform.position,
                    out RaycastHit hit,
                    _physicsMask
                ))
                {
                    if (hit.collider == overlapCollider)
                    {
                        inSight = true;
                        hitPoint = hit.point;
                    }
                }

                if (inSight)
                {
                    var distance = Vector3.Distance(bombPosition, hitPoint);
                    float damageRatio = 1f - Mathf.Clamp01(distance / ExplosionRadius);
                    damageable.Damage(_explosionDamage * damageRatio);
                }
            }

            DetonatedEvent?.Invoke();
            Destroy(gameObject);
        }
    }
}