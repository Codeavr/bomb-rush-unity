﻿using System;
using UnityEngine;

namespace BombRush.Bombs
{
    public class BombExplosionEffect : MonoBehaviour
    {
        [SerializeField]
        private BombBehaviour _bomb;

        [SerializeField]
        private GameObject _effectPrototype;

        private void Awake()
        {
            _bomb.DetonatedEvent += OnDetonated;
        }

        private void OnDestroy()
        {
            if (_bomb != null)
            {
                _bomb.DetonatedEvent -= OnDetonated;
            }
        }

        private void OnDetonated()
        {
            var effect = Instantiate(_effectPrototype, transform.position, Quaternion.identity);
            effect.transform.localScale = _bomb.ExplosionRadius * 2f * Vector3.one;
        }
    }
}