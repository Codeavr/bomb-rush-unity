﻿using BombRush.Core;
using BombRush.Utilities;
using Codeavr.RandomExtensions;
using UnityEngine;

namespace BombRush.Bombs.Spawner
{
    public class BombsSpawner : ITickable
    {
        private readonly BombSpawnerSettingsSO _settings;

        private float _currentTime;
        private float _nextSpawnTime;

        public BombsSpawner(BombSpawnerSettingsSO settings)
        {
            _settings = settings;
        }

        void ITickable.Tick(float deltaTime)
        {
            _currentTime += deltaTime;

            while (_currentTime > _nextSpawnTime)
            {
                SpawnBomb();
                _nextSpawnTime += _settings.SpawnDelay;
            }
        }

        private void SpawnBomb()
        {
            Vector3 position = _settings.SpawnRect.RandomPoint().ToV3(_settings.SpawnHeight);

            BombBehaviour bomb = Object.Instantiate(_settings.BombPrototype, position, Quaternion.identity);
            bomb.Initialize(_settings.ExplosionDamage, _settings.ExplosionRadius, _settings.ExplosionCollisionMask);
        }
    }
}