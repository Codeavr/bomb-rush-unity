﻿using UnityEngine;

namespace BombRush.Bombs.Spawner
{
    [CreateAssetMenu(fileName = nameof(BombSpawnerSettingsSO), menuName = "BombRush/" + nameof(BombSpawnerSettingsSO))]
    public class BombSpawnerSettingsSO : ScriptableObject
    {
        [SerializeField]
        private BombBehaviour _bombPrototype;

        [SerializeField]
        private float _spawnDelay;

        [SerializeField]
        private Rect _spawnRect;

        [SerializeField]
        private float _spawnHeight;

        [SerializeField]
        private float _explosionDamage;

        [SerializeField]
        private float _explosionRadius;

        [SerializeField]
        private LayerMask _explosionCollisionMask;

        public BombBehaviour BombPrototype => _bombPrototype;
        public float SpawnDelay => _spawnDelay;

        public Rect SpawnRect => _spawnRect;

        public float SpawnHeight => _spawnHeight;

        public float ExplosionDamage => _explosionDamage;

        public float ExplosionRadius => _explosionRadius;

        public LayerMask ExplosionCollisionMask => _explosionCollisionMask;
    }
}