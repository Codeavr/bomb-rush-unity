﻿using UnityEngine;

namespace BombRush.Utilities
{
    public static class VectorExtensions
    {
        public static Vector3 ToV3(this Vector2 vec2, float y = 0)
        {
            return new Vector3(vec2.x, y, vec2.y);
        }
    }
}