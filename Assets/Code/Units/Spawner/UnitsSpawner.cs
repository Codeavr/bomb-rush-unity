﻿using BombRush.Bombs;
using BombRush.Core;
using BombRush.Utilities;
using Codeavr.RandomExtensions;
using UnityEngine;

namespace BombRush.Units.Spawner
{
    public class UnitsSpawner : ITickable
    {
        private const int MaxPositionSearchAttempts = 128;

        private float _currentTime;
        private float _nextSpawnTime;
        private UnitSpawnerSettingsSO _settings;

        public UnitsSpawner(UnitSpawnerSettingsSO settings)
        {
            _settings = settings;
        }

        void ITickable.Tick(float deltaTime)
        {
            _currentTime += deltaTime;

            while (_currentTime > _nextSpawnTime)
            {
                SpawnBomb();
                _nextSpawnTime += _settings.SpawnDelay;
            }
        }

        private void SpawnBomb()
        {
            Vector3 position = RollPosition();
            for (int i = 0; i < MaxPositionSearchAttempts; i++)
            {
                if (!Physics.CheckSphere(position, _settings.UnitPrototype.transform.localScale.x / 3f))
                {
                    break;
                }

                position = RollPosition();
            }

            UnitBehaviour unit = Object.Instantiate(_settings.UnitPrototype, position, Quaternion.identity);

            unit.Initialize(_settings.MaxHealth);
        }

        private Vector3 RollPosition()
        {
            return _settings.SpawnRect.RandomPoint().ToV3(_settings.SpawnHeight);
        }
    }
}