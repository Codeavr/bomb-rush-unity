﻿using UnityEngine;

namespace BombRush.Units.Spawner
{
    [CreateAssetMenu(fileName = nameof(UnitSpawnerSettingsSO), menuName = "BombRush/" + nameof(UnitSpawnerSettingsSO))]
    public class UnitSpawnerSettingsSO : ScriptableObject
    {
        [SerializeField]
        private UnitBehaviour _unitPrototype;

        [SerializeField]
        private float _spawnDelay;

        [SerializeField]
        private Rect _spawnRect;

        [SerializeField]
        private float _spawnHeight;

        [SerializeField]
        private float _maxHealth;

        public UnitBehaviour UnitPrototype => _unitPrototype;
        public float SpawnDelay => _spawnDelay;
        public Rect SpawnRect => _spawnRect;
        public float SpawnHeight => _spawnHeight;
        public float MaxHealth => _maxHealth;
    }
}