﻿using BombRush.Core;
using UnityEngine;

namespace BombRush.Units
{
    public class UnitBehaviour : MonoBehaviour, IDamageable
    {
        private float _currentHealth;

        public void Initialize(float maxHealth)
        {
            _currentHealth = maxHealth;
        }

        public void Damage(float damage)
        {
            _currentHealth = Mathf.Max(0, _currentHealth - damage);

            if (_currentHealth < Mathf.Epsilon)
            {
                Destroy(gameObject);
            }
        }
    }
}