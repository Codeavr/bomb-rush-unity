﻿namespace BombRush.Core
{
    interface ITickable
    {
        void Tick(float deltaTime);
    }
}