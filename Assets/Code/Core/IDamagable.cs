﻿namespace BombRush.Core
{
    public interface IDamageable
    {
        void Damage(float damage);
    }
}