﻿using System;
using System.Collections.Generic;
using BombRush.Bombs.Spawner;
using BombRush.Core;
using BombRush.Units.Spawner;
using UnityEngine;

namespace BombRush
{
    public class Bootstrap : MonoBehaviour
    {
        [SerializeField]
        private BombSpawnerSettingsSO _bombSpawnerSettings;

        [SerializeField]
        private UnitSpawnerSettingsSO _unitSpawnerSettings;

        private List<ITickable> _tickables = new List<ITickable>();

        private void Start()
        {
            var bombsSpawner = new BombsSpawner(_bombSpawnerSettings);
            var unitsSpawner = new UnitsSpawner(_unitSpawnerSettings);

            _tickables.Add(bombsSpawner);
            _tickables.Add(unitsSpawner);
        }

        private void Update()
        {
            var deltaTime = Time.deltaTime;

            for (var i = 0; i < _tickables.Count; i++)
            {
                _tickables[i].Tick(deltaTime);
            }
        }
    }
}